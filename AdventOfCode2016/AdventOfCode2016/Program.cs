﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2016
{
   internal static class Program
   {
      private static void Main(string[] args)
      {
         //Day1.Process();
         //Day2.Process();
         //Day3.Process();
         //Day4.Process();
         //Day5.Process();
         //Day6.Process();
         //Day7.Process();
         //Day8.Process();
         Day9.Process();
         Console.ReadKey();
      }
   }
}
