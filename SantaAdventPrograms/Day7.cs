﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SantaAdventPrograms
{
   static class Day7
   {
      private class InstructionResult
      {
         public string InputA { get; set; }
         public string InputB { get; set; }
         public string Command { get; set; }
         public int? Value { get; set; }
      }

      //This problem is better solved with recursion. I hate recursion!!!!!
      public static void Process()
      {
         var inputFile = File.ReadLines(Path.Combine("Inputs", "Day7Input.txt")).ToArray();

         var instructionSet = new Dictionary<string, InstructionResult>();

         foreach (var line in inputFile.Select(c => c.Split()))
         {
            if (line.Count() == 3)
            {
               int temp;
               int.TryParse(line[0], out temp);
               var tempA = Regex.IsMatch(line[0], @"^[a-zA-Z]") ? line[0] : "0";
               if (line[2].Equals("b"))
                  temp = 956;
                     
               instructionSet.Add(line[2], new InstructionResult {InputA = tempA, InputB = "0", Command = "INIT", Value = temp});
            }
            else if (line[0].Equals("NOT"))
            {
               instructionSet.Add(line[3], new InstructionResult {InputA = line[1], InputB = "0", Command = line[0], Value = null});
            }
            else
            {
               instructionSet.Add(line[4], new InstructionResult {InputA = line[0], InputB = line[2], Command = line[1], Value = null});
            }
         }

         var rcount = instructionSet.Count(c => c.Value.Value == null);

         while (rcount > 0)
         {
            foreach (var key in instructionSet.Select(c => c).Where(v => v.Value.Value != null 
                                                                  || (!Regex.IsMatch(v.Value.InputA, @"^[a-zA-Z]") && !Regex.IsMatch(v.Value.InputB, @"^[a-zA-Z]"))))
            {
               if (key.Value.Command.Trim() == "NOT" && key.Value.InputA.All(char.IsDigit))
               {
                  key.Value.Value = Calculate(key.Value.InputA, key.Value.InputB, key.Value.Command.Trim());

                  EnterWireSetting(key.Key, key.Value.Value, instructionSet);
                  continue;
               }

               if (Regex.IsMatch(key.Value.InputA, @"^[a-zA-Z]") || Regex.IsMatch(key.Value.InputB, @"^[a-zA-Z]"))
                  continue;

               if (!key.Value.Command.Equals("INIT"))
                  key.Value.Value = Calculate(key.Value.InputA, key.Value.InputB, key.Value.Command.Trim());

               EnterWireSetting(key.Key, key.Value.Value, instructionSet);
            }
           rcount = instructionSet.Count(c => c.Value.Value == null);
         }

         Console.WriteLine("******* Day 7 ********");
         Console.WriteLine("Wire a = " + instructionSet["a"].Value);
      }

      private static int ParseOutInput(string inputString)
      {
         int result;

         int.TryParse(inputString, out result);

         return result;
      }

      private static void EnterWireSetting(string calcKey, int? calcValue, Dictionary<string, InstructionResult> instructionSet)
      {
         foreach (var wire in instructionSet.Select(c => c).Where(v => v.Value.InputA.Trim() == calcKey || v.Value.InputB.Trim() == calcKey))
         {
            if (wire.Value.Command.Equals("INIT"))
               wire.Value.Value = calcValue;
            if (wire.Value.InputA.Trim() == calcKey)
               wire.Value.InputA = calcValue.ToString();
            if (wire.Value.InputB.Trim() == calcKey)
               wire.Value.InputB = calcValue.ToString();
         }
      }

      private static int Calculate(string partA, string partB, string command)
      {
         var result = 0;

         var inputA = ParseOutInput(partA);
         var inputB = ParseOutInput(partB);

         if (command.Equals("AND"))
         {
            result = inputA & inputB;
         }
         else if (command.Equals("OR"))
         {
            result = inputA | inputB;
         }
         else if (command.Equals("NOT"))
         {
            result = ~inputA;
         }
         else if (command.Equals("LSHIFT"))
         {
            result = inputA << inputB;
         }
         else if (command.Equals("RSHIFT"))
         {
            result = inputA >> inputB;
         }

         return result;
      }
   }
}

/*
--- Day 7: Some Assembly Required ---

This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates! Unfortunately, little Bobby is a little under the recommended age range, and he needs help assembling the circuit.

Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535). A signal is provided to each wire by a gate, another wire, or some specific value. Each wire can only get a signal from one source, but can provide its signal to multiple destinations. A gate provides no signal until all of its inputs have a signal.

The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y to an AND gate, and then connect its output to wire z.

For example:

123 -> x means that the signal 123 is provided to wire x.
x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.
Other possible gates include OR (bitwise OR) and RSHIFT (right-shift). If, for some reason, you'd like to emulate the circuit instead, almost all programming languages (for example, C, JavaScript, or Python) provide operators for these gates.

For example, here is a simple circuit:

123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
After it is run, these are the signals on the wires:

d: 72
e: 507
f: 492
g: 114
h: 65412
i: 65079
x: 123
y: 456
In little Bobby's kit's instructions booklet (provided as your puzzle input), what signal is ultimately provided to wire a?

Your puzzle answer was 956.

--- Part Two ---

Now, take the signal you got on wire a, override wire b to that signal, and reset the other wires (including wire a). What new signal is ultimately provided to wire a?

Your puzzle answer was 40149.
 */
