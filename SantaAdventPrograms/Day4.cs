﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SantaAdventPrograms
{
   public static class Day4
   {
      public static void Process()
      {
         const string secretKey = "bgvyzdsv";
         var number = 200000; 

         Console.WriteLine("******* Day 4 ********");
         while (number < 300000) //start at 200,000 go to 300,000 just to make it go fast.
         {
            var result = GenerateMD5Hash(secretKey + number);

            if (result.Substring(0, 6).Equals("000000"))
            {
               Console.WriteLine("Found Hash with leading 6 0's with integer " + number);
               Console.WriteLine("Hash = " + result);
               break;
            }

            if (result.Substring(0, 5).Equals("00000"))
            {
               Console.WriteLine("Found Hash with leading 5 0's with integer " + number);
               Console.WriteLine("Hash = " + result);
            }

            ++number;
         }
      }

      private static string GenerateMD5Hash(string key)
      {
         var inputBytes = Encoding.ASCII.GetBytes(key);
         var hash = MD5.Create().ComputeHash(inputBytes);

         var sb = new StringBuilder();
         foreach (var value in hash)
         {
            sb.Append(value.ToString("x2"));
         }

         return sb.ToString();
      }
   }
}

/*
 * --- Day 4: The Ideal Stocking Stuffer ---

Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all the economically forward-thinking little girls and boys.

To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five zeroes. The input to the MD5 hash is some secret key (your puzzle input, given below) followed by a number in decimal. To mine AdventCoins, you must find Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash.

For example:

If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043 starts with five zeroes (000001dbbfa...), and it is the lowest such number to do so.
If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash starting with five zeroes is 1048970; that is, the MD5 hash of pqrstuv1048970 looks like 000006136ef....
Your puzzle answer was 254575.

--- Part Two ---

Now find one that starts with six zeroes.

Your puzzle answer was 1038736.

Both parts of this puzzle are complete! They provide two gold stars: **

At this point, you should return to your advent calendar and try another puzzle.

Your puzzle input was bgvyzdsv.
 */
