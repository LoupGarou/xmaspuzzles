﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SantaAdventPrograms
{
   public static class Day5
   {
      public static void Process()
      {
         var inputFile = File.ReadLines(Path.Combine("Inputs", "Day5Input.txt")).ToArray();
         OldWay(inputFile);
         NewWay(inputFile);
      }

      private static void NewWay(IEnumerable<string> inputFile)
      {
         var naughty = 0;
         var nice = 0;

         foreach (var value in inputFile)
         {
            if (FindPairs(value) && RepeatingLetter(value))
            {
               ++nice;
            }
            else
            {
               ++naughty;
            }
         }

         Console.WriteLine("******* Day 5 (New way) ********");
         Console.WriteLine("Number of nice strings    = " + nice);
         Console.WriteLine("Number of naughty strings = " + naughty); 
      }

      private static bool FindPairs(string value)
      {
         for (var index = 0; index < value.Length - 1; ++index)
         {
            var temp = value[index].ToString() + value[index + 1];
            var found = PairCount(value, index + 2, temp);
            if (found != -1)
               return true;
         }

         return false;
      }

      private static int PairCount(string value, int index, string pattern)
      {
         return value.IndexOf(pattern, index, StringComparison.Ordinal);
      }

      private static bool RepeatingLetter(string value)
      {
         for (var index = 0; index < value.Length - 2; ++index)
         {
            if (value[index] == value[index + 2])
               return true;
         }

         return false;
      }

      private static void OldWay(IEnumerable<string> inputFile)
      {
         var naughty = 0;
         var nice = 0;
         var badList = new List<string> { "ab", "cd", "pq", "xy" };

         foreach (var value in inputFile)
         {
            if (InBadList(value, badList))
               ++naughty;
            else
            {
               if (!InVowelList(value))
                  ++naughty;
               else if (HasDoubleLetters(value))
                  ++nice;
               else
                  ++naughty;
            }
         }

         Console.WriteLine("******* Day 5 (Old way) ********");
         Console.WriteLine("Number of nice strings    = " + nice);
         Console.WriteLine("Number of naughty strings = " + naughty); 
      }

      private static bool InVowelList(string value)
      {

         var vowelCount = 0;

         vowelCount += value.Count(c => c == 'a');
         vowelCount += value.Count(c => c == 'e');
         vowelCount += value.Count(c => c == 'i');
         vowelCount += value.Count(c => c == 'o');
         vowelCount += value.Count(c => c == 'u');

         return (vowelCount > 2);
      }

      private static bool HasDoubleLetters(string value)
      {
         for (var index = 0; index < value.Length - 1; ++index)
         {
            if (value[index] == value[index + 1])
               return true;
         }

         return false;
      }

      private static bool InBadList(string value, IEnumerable<string> badList)
      {
         return badList.Any(value.Contains);
      }
   }
}

/*
--- Day 5: Doesn't He Have Intern-Elves For This? ---

Santa needs help figuring out which strings in his text file are naughty or nice.

A nice string is one with all of the following properties:

It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
For example:

ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
jchzalrnumimnmhp is naughty because it has no double letter.
haegwjzuvuyypxyu is naughty because it contains the string xy.
dvszwmarrgswjxmb is naughty because it contains only one vowel.
How many strings are nice?

Your puzzle answer was 255.

--- Part Two ---

Realizing the error of his ways, Santa has switched to a better model of determining whether a string is naughty or nice. None of the old rules apply, as they are all clearly ridiculous.

Now, a nice string is one with all of the following properties:

It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
For example:

qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.
How many strings are nice under these new rules?

Your puzzle answer was 55.
 */
