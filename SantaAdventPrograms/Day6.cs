﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SantaAdventPrograms
{
   static class Day6
   {
      class LightCoordinates
      {
         public int Row { get; set; }
         public int Column { get; set; }
         public int OnOff { get; set; }
         public int Brightness { get; set; }
      }

      public static void Process()
      {

         //This is a tad slow. Think of a more efficient way to accomplish this.
         var lightList = new Dictionary<string, LightCoordinates>();

         var inputFile = File.ReadLines(Path.Combine("Inputs", "Day6Input.txt")).ToArray();
         //var inputFile = new[] { "turn on 0,0 through 9,0"}; 

         InitializeLightList(lightList);

         foreach (var command in inputFile.Select(line => line.ToLower().Split()))
         {
            if (command[0].Equals("toggle"))
            {
               AdjustLight(lightList, command[0], command[1].Split(','), command[3].Split(','));
            }
            else if (command[0].Equals("turn"))
            {
               AdjustLight(lightList, command[1], command[2].Split(','), command[4].Split(','));
            }
         }

         Console.WriteLine("******* Day 6 ********");
         Console.WriteLine("Number of lit lights = " + lightList.Count(c => c.Value.OnOff == 1));
         Console.WriteLine("Total brightness level of lit lights = " + lightList.Sum(c => c.Value.Brightness));

      }

      private static void AdjustLight(IReadOnlyDictionary<string, LightCoordinates> lightArray, string command, IReadOnlyList<string> startLight, IReadOnlyList<string> endLight)
      {
         var startX = 0;
         var startY = 0;
         var endX = 0;
         var endY = 0;

         int.TryParse(startLight[0], out startX);
         int.TryParse(startLight[1], out startY);
         int.TryParse(endLight[0], out endX);
         int.TryParse(endLight[1], out endY);

         while (startX <= endX)
         {
            var indexY = startY;
            while (indexY <= endY)
            {
               var index = string.Format("{0:000}{1:000}", startX, indexY); 
               if (command.Equals("toggle"))
               {
                  lightArray[index].Brightness += 2;
                  lightArray[index].OnOff = lightArray[index].OnOff == 1 ? 0 : 1;
               }
               else if (command.Equals("on"))
               {
                  lightArray[index].Brightness += 1;
                  lightArray[index].OnOff = 1;
               }
               else if (command.Equals("off"))
               {
                  lightArray[index].Brightness -= (lightArray[index].Brightness > 0) ? 1 : 0;
                  lightArray[index].OnOff = 0;
               }
               ++indexY;
            }

            ++startX;
         }
      }

      private static void InitializeLightList(IDictionary<string, LightCoordinates> lightList)
      {
         for (var row = 0; row < 1000; ++row)
         {
            for (var col = 0; col < 1000; ++col)
            {
               var index = string.Format("{0:000}{1:000}", row, col);
               lightList.Add(index, new LightCoordinates { Row = row, Column = col, OnOff = 0, Brightness = 0});
            }
         } 
      }
   }
}

/*
--- Day 6: Probably a Fire Hazard ---

Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

For example:

turn on 0,0 through 999,999 would turn on (or leave on) every light.
toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
After following the instructions, how many lights are lit?

Your puzzle answer was 543903.

--- Part Two ---

You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

The phrase turn on actually means that you should increase the brightness of those lights by 1.

The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

The phrase toggle actually means that you should increase the brightness of those lights by 2.

What is the total brightness of all lights combined after following Santa's instructions?

For example:

turn on 0,0 through 0,0 would increase the total brightness by 1.
toggle 0,0 through 999,999 would increase the total brightness by 2000000.
Your puzzle answer was 14687245.
 */
