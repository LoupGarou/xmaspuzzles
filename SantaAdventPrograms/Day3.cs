﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SantaAdventPrograms
{
   public static class Day3
   {
      class MapCoordinates
      {
         public int XAxisCoordinate { get; set; }
         public int YAxisCoordinate { get; set; }
         public int Delivered { get; set; }
      }

      public static void Process()
      {
         int nsIndicator;
         int ewIndicator;

         var nsIndicatorSanta = 0;
         var ewIndicatorSanta = 0;
         var nsIndicatorRobo = 0;
         var ewIndicatorRobo = 0;

         var santaDelivered = false;

         var deliveryMap = new List<MapCoordinates> { new MapCoordinates() { XAxisCoordinate = 0, YAxisCoordinate = 0, Delivered = 2 } };

         var inputFile = File.ReadAllText(Path.Combine("Inputs", "Day3Input.txt")).Replace("\r", "").Replace("\n", "").ToCharArray();

         foreach (var direction in inputFile)
         {
            santaDelivered = (santaDelivered != true);

            nsIndicator = (santaDelivered) ? nsIndicatorSanta : nsIndicatorRobo;
            ewIndicator = (santaDelivered) ? ewIndicatorSanta : ewIndicatorRobo;

            Travel(direction, ref nsIndicator, ref ewIndicator);

            var alreadyVisited = deliveryMap.FirstOrDefault(c => c.XAxisCoordinate == ewIndicator && c.YAxisCoordinate == nsIndicator);

            if (alreadyVisited != null)
            {
               ++alreadyVisited.Delivered;
            }
            else
            {
               deliveryMap.Add(new MapCoordinates { XAxisCoordinate = ewIndicator, YAxisCoordinate = nsIndicator, Delivered = 1 });
            }

            if (santaDelivered)
            {
               nsIndicatorSanta = nsIndicator;
               ewIndicatorSanta = ewIndicator;
            }
            else
            {
               nsIndicatorRobo = nsIndicator;
               ewIndicatorRobo = ewIndicator;
            }
         }

         Console.WriteLine("******* Day 3 ********");
         Console.WriteLine("Number of houses receiving at least 1 present   = " + deliveryMap.Count());
         Console.WriteLine("Number of houses receiving exactly 2 presents   = " + deliveryMap.Count(c => c.Delivered == 2));
         Console.WriteLine("Number of houses receiving more than 2 presents = " + deliveryMap.Count(c => c.Delivered > 2));
      }

      private static void Travel(char direction, ref int xCoord , ref int yCoord)
      {
         switch (direction)
         {
            case '^':
               ++xCoord;
               break;
            case 'v':
               --xCoord;
               break;
            case '>':
               ++yCoord;
               break;
            case '<':
               --yCoord;
               break;
            default:
               throw new ArgumentException("WARNING!!! Santa is trying to traverse time!");
         } 
      }
   }
}

/*
 * --- Day 3: Perfectly Spherical Houses in a Vacuum ---

Santa is delivering presents to an infinite two-dimensional grid of houses.

He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west (<). After each move, he delivers another present to the house at his new location.

However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and Santa ends up visiting some houses more than once. How many houses receive at least one present?

For example:

> delivers presents to 2 houses: one at the starting location, and one to the east.
^>v< delivers presents to 4 houses in a square, including twice to the house at his starting/ending location.
^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.
Your puzzle answer was 2572.

--- Part Two ---

The next year, to speed up the process, Santa creates a robot version of himself, Robo-Santa, to deliver presents with him.

Santa and Robo-Santa start at the same location (delivering two presents to the same starting house), then take turns moving based on instructions from the elf, who is eggnoggedly reading from the same script as the previous year.

This year, how many houses receive at least one present?

For example:

^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they started.
^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and Robo-Santa going the other.
Your puzzle answer was 2631.
 */
