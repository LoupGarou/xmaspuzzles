﻿using System;

namespace SantaAdventPrograms
{
   static class Program
   {
      static void Main(string[] args)
      {
         Day1.Process();
         Day2.Process();
         Day3.Process();
         Day4.Process();
         Day5.Process();
         Day6.Process(); //Takes quite some time to execute
         Day7.Process();
         Day8.Process();
         //Day9.Process();
         Day10.Process();
         Console.ReadKey();
      }
   }
}
