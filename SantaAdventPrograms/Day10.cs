﻿using System;
using System.Text;

namespace SantaAdventPrograms
{
   static class Day10
   {
      public static void Process()
      {
         const string inputFile = "1113122113";
         //var inputFile = "11";

         var result = new StringBuilder(inputFile);

         for (var count = 0; count < 40; ++count)
         {
            result = SeeAndSay(result);
         }
         Console.WriteLine("******* Day 10 ********");
         Console.WriteLine("Puzzle length = " + result.Length);
      }

      private static StringBuilder SeeAndSay(StringBuilder inputFile)
      {
         var result = new StringBuilder();
         var temp = new StringBuilder(inputFile[0].ToString());

         for (var count = 0; count < inputFile.Length; ++count)
         {
            if (count == inputFile.Length - 1)
            {
               if (inputFile[count].Equals(temp[0]))
               {
                  result.Append((temp.Length + 1).ToString() + temp[0]);
               }
               else
               {
                  result.Append(temp.Length.ToString() + temp[0]).Append("1" + inputFile[count]);
                  continue;
               }
            }

            if (count == 0)
               continue;

            if (inputFile[count].Equals(temp[0]))
            {
               temp.Append(inputFile[count]);
               continue;
            }

            result.Append(temp.Length.ToString() + temp[0]);
            temp.Clear();
            temp.Append(inputFile[count]);
         }

         return result;
      }
   }
}
/*
--- Day 10: Elves Look, Elves Say ---

Today, the Elves are playing a game called look-and-say. They take turns making sequences by reading aloud the previous sequence and using that reading as the next sequence. For example, 211 is read as "one two, two ones", which becomes 1221 (1 2, 2 1s).

Look-and-say sequences are generated iteratively, using the previous value as input for the next step. For each step, take the previous value, and replace each run of digits (like 111) with the number of digits (3) followed by the digit itself (1).

For example:

1 becomes 11 (1 copy of digit 1).
11 becomes 21 (2 copies of digit 1).
21 becomes 1211 (one 2 followed by one 1).
1211 becomes 111221 (one 1, one 2, and two 1s).
111221 becomes 312211 (three 1s, two 2s, and one 1).
Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?

Your puzzle answer was 360154.

--- Part Two ---

Neat, right? You might also enjoy hearing John Conway talking about this sequence (that's Conway of Conway's Game of Life fame).

Now, starting again with the digits in your puzzle input, apply this process 50 times. What is the length of the new result?

Your puzzle answer was 5103798.

Both parts of this puzzle are complete! They provide two gold stars: **

At this point, you should return to your advent calendar and try another puzzle.

Your puzzle input was 1113122113.
*/